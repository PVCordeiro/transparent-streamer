## Transparent Streamer

This script is meant to stream in a transparent way a MPEG-TS file through an UDP socket. It is simple, just calculate the Program Clock Referecence for each packet, then multicast each packet as-is through the socket interface, then waint the PCR of each packet.

This script is the very first version, so it is underdeveloped. Below a list of its limitations:

1. Output address is hardcoded as 239.128.0.1:5004.

2. No check of sync byte (0x47) on multicaster loop.

3. No input file check.

---

## License

Copyright (c) 2021 ZAPPWARE NV 
Project developed by: Paulo Vinícius M. Cordeiro <paulo.cordeiro@zappware.com>
Software licensed under GPLv3 terms and conditions, see LICENSE file.
