#!/usr/bin/python3
# Copyright (c) 2021 ZAPPWARE NV <paulo.cordeiro@zappware.com>
# Software licensed under GPLv3 terms and conditions, see LICENSE file.

import socket
import time
import argparse
import pdb
import sys
import binascii
import struct
from time import sleep

MULTICAST_TTL = 2
MCAST_GRP = '239.128.0.1'
MCAST_PORT = 5004
SLEEP_GUARD_TIME = 0.00
MPEG_PKT_SIZE = 188
pcr_pid = int("0x2000", 16) # This is a value out of pid's range

ap = argparse.ArgumentParser(description='Multicast the input stream transparent to (default) udp://239.128.0.1:5004')
ap.add_argument('-i', '--input',required=True,help='mpg file')
ap.add_argument('-I', '--infinit',default=False,action='store_true',help='the stream runs forever')
args = vars(ap.parse_args())

input_file=args['input']
infinit=args['infinit']

pcr_list = {}

def pop_pkt_counter():
    sys.stdout.write("Package counter: " + "0"*8)
    sys.stdout.flush()

def print_counter(c):
    sys.stdout.write('\b'*8)
    sys.stdout.write("{:8}".format(c))
    sys.stdout.flush()

def get_now():
    return time.perf_counter()

def my_sleep(duration):
    time.sleep(duration)

def PCRCalc(pl6bytes):
    pcr_headerse = int(pl6bytes[0])
    pcr_headerse = (pcr_headerse << 8) + int(pl6bytes[1])
    pcr_headerse = (pcr_headerse << 8) + int(pl6bytes[2])
    pcr_headerse = (pcr_headerse << 8) + int(pl6bytes[3])
    pcr_headerse = (pcr_headerse << 1) + int(pl6bytes[4] >> 7)
    pcr_ext = (int (pl6bytes[4] & 1) << 8) + (pl6bytes[5])
    return (pcr_headerse * 300 + pcr_ext) / 27000000

def getPCRPid():
    return pcr_pid

def ifPmtStorePcrPid(pyl):
    #    MPEG2 Program Map Table
    #    Table ID: Program Map Table (PMT) (0x02)
    #    1... .... .... .... = Syntax indicator: 1
    #    .011 .... .... .... = Reserved: 0x3
    #    .... 0000 0010 0011 = Length: 35
    #    Program Number: 0x0001
    #    11.. .... = Reserved: 0x3
    #    ..00 011. = Version Number: 0x03
    #    .... ...1 = Current/Next Indicator: Currently applicable (0x1)
    #    Section Number: 0
    #    Last Section Number: 0
    #    111. .... .... .... = Reserved: 0x7
    #    ...0 0001 0000 0000 = PCR PID: 0x0100
    #    1111 .... .... .... = Reserved: 0xf
    #    .... 0000 0000 0110 = Program Info Length: 0x006
    #    Descriptor Tag=0x09
    #    Stream PID=0x0100
    #    Stream PID=0x0101
    #    CRC 32: 0x7645a789 [unverified]
    #    [CRC 32 Status: Unverified]
    if pyl[1] == 2: # PMT pid is 0x2
        global pcr_pid
        pcr_pid = ((pyl[9] & 31) << 8) + pyl[10] # int('1FFF',16)

def parsePID(header):
    return ((header[1] & 31) << 8) + header[2]

def isAdaptationField(header):
    return header[3] & 32

def timestamp4packet():
    with open(input_file, "rb") as f:
        header = 0
        count = 0
        pcr_list[0] = 0.0

        #Example of MPEG Header with Adaptation field
        #ISO/IEC 13818-1 PID=0x100 CC=15
        #    Header: 0x4701002f
        #        0100 0111 .... .... .... .... .... .... = Sync Byte: Correct (0x47)
        #        .... .... 0... .... .... .... .... .... = Transport Error Indicator: 0
        #        .... .... .0.. .... .... .... .... .... = Payload Unit Start Indicator: 0
        #        .... .... ..0. .... .... .... .... .... = Transport Priority: 0
        #        .... .... ...0 0001 0000 0000 .... .... = PID: Unknown (0x0100)
        #        .... .... .... .... .... .... 00.. .... = Transport Scrambling Control: Not scrambled (0x0)
        #        .... .... .... .... .... .... ..10 .... = Adaptation Field Control: Adaptation Field only (0x2)
        #        .... .... .... .... .... .... .... 1111 = Continuity Counter: 15
        #    [MPEG2 PCR Analysis]
        #    Adaptation Field Length: 183
        #    Adaptation Field
        #        0... .... = Discontinuity Indicator: 0
        #        .0.. .... = Random Access Indicator: 0
        #        ..0. .... = Elementary Stream Priority Indicator: 0
        #        ...1 .... = PCR Flag: 1
        #        .... 0... = OPCR Flag: 0
        #        .... .0.. = Splicing Point Flag: 0
        #        .... ..0. = Transport Private Data Flag: 0
        #        .... ...0 = Adaptation Field Extension Flag: 0
        #        Program Clock Reference: 0x00000000000b4c87
        #        Stuffing: ffffffffffffffffffffffffffffffffffffffffffffffff...

        while header != '\0':
            header = f.read(4)
            try:
                if header[0] != 71: # int("0x47", 16):
                    print("!!!! NOT A SYNC BYTE !!!")
                    continue
            except:
                break
            
            payload = f.read(MPEG_PKT_SIZE - 4)

            # if this pkt is a PMT, so get the pcr pid and store it in a global var
            ifPmtStorePcrPid(payload)
    
            # compare if this header has the adaptation fiels,
            # then check if the pid is 0x100
            pid = parsePID(header)
            if ( isAdaptationField(header) and pid == getPCRPid() ):

                adaptation_field_size = payload[0]
                if ((adaptation_field_size > 6) and (payload[1] & 16)):
                    pcr_list[count] = PCRCalc(payload[2:8])
            count += 1
        f.close()
    
    last_k = -1
    pcr_tmp = pcr_list.copy()
    for k, v in pcr_tmp.items():
        if (last_k + 1) < k:
            pace = (v - pcr_list[last_k])/(k-last_k)
    
            while (last_k + 1) < k:
                pcr_list[last_k + 1] = pcr_list[last_k] + pace
                last_k += 1
        last_k = k

def print_pcrs():
    lastv = 0.0
    for k in sorted(pcr_list.keys()):
        v = pcr_list[k]
        if ((v - lastv) > 0.1):
            sys.stdout.write("ATENTION: ")
        if k in pcr_tmp:
            print ("- " + str(k) +":" + str(v))
        else:
            print (str(k) +":" + str(v))
        lastv = v

def multicastTs():
    with open(input_file, "rb") as f:
        count = 0
        payload = bytearray([])
        pop_pkt_counter()
    
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)
    
        start_time = get_now()
        while True:
            
            payload += bytearray(f.read(MPEG_PKT_SIZE))
            if payload == '\0':
                break
            print_counter(count)
    
            if (count % 7) == 0:
                now = get_now()
                sock.sendto(payload, (MCAST_GRP, MCAST_PORT))
                payload.clear()
    
                time_to_wait = (pcr_list[count + 1] + start_time) - get_now() - SLEEP_GUARD_TIME
                #print("--> " + str(pcr_list[count]) + " + " + str(start_time) + " - " + str(now) + " = " + str(time_to_wait))
                if time_to_wait > 1:
                    print (" pcr calc fail:" + str(time_to_wait))
                if time_to_wait > 0 and time_to_wait < 5:
                    my_sleep(time_to_wait)
                else:
                    print (" faulty pcr:" + str(time_to_wait))
    
            count += 1
        f.close()


def main():
    timestamp4packet()
    while True:
        try:
            multicastTs()
        except KeyboardInterrupt:
            print("\nFinishing stream by keyboard interruption")
            break
        except Exception as e:
            print(str(e))
        if not infinit:
            break

if __name__ == "__main__":
    main()
